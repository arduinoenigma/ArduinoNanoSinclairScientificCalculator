#include "GPIO.h"

//
// 0 a g d b c f e
// 0 1 1 1 1 1 1 1 // digit
// 1 1 1 1 1 1 1 0 // shift left
// 0 1 1 0 1 0 1 0 // and with mask
// 0 1 1 0 1 0 1 0 // digit scrolled one step up
//
// 0 a g d b c f e
// 0 1 1 1 1 1 1 1 // digit
// 0 0 1 1 1 1 1 1 // shift right
// 0 0 1 1 0 1 0 1 // and with mask
// 0 0 1 1 0 1 0 1 // digit scrolled one step dn
//

const byte DIGITSTABLEDN[] PROGMEM = {
  95, 42, 64, 4,
  12, 8, 16, 52,
  121, 98, 80, 52,
  124, 104, 64, 21,
  46, 72, 16, 49,
  118, 104, 80, 49,
  119, 106, 80, 36,
  76, 8, 16, 53,
  127, 106, 80, 53,
  126, 104, 80, 37
};

const byte DIGITSTABLEUP[] PROGMEM = {
  95, 37, 16, 8,
  12, 4, 64, 98,
  121, 52, 80, 104,
  124, 52, 16, 72,
  46, 21, 64, 104,
  118, 49, 80, 106,
  119, 49, 16, 8,
  76, 36, 80, 106,
  127, 53, 80, 104,
  126, 53, 80, 42
};


GPIO<BOARD::D5>  SegmentA;
GPIO<BOARD::D8>  SegmentB;
GPIO<BOARD::D10> SegmentC;
GPIO<BOARD::D19> SegmentD;
GPIO<BOARD::D9>  SegmentE;
GPIO<BOARD::D6>  SegmentF;
GPIO<BOARD::D14> SegmentG;
GPIO<BOARD::D18> SegmentDP;

GPIO<BOARD::D2>  Digit1;
GPIO<BOARD::D3>  Digit2;
GPIO<BOARD::D4>  Digit3;
GPIO<BOARD::D17> Digit4;
GPIO<BOARD::D16> Digit5;
GPIO<BOARD::D7>  Digit6;
GPIO<BOARD::D15> Digit7;
GPIO<BOARD::D11> Digit8;
GPIO<BOARD::D12> Digit9;

#define SegmentOn low
#define SegmentOff high
#define DigitOn high
#define DigitOff low
#define IsKeyPushed > 100

void allSegmentOutput() {
  SegmentA.output();
  SegmentB.output();
  SegmentC.output();
  SegmentD.output();
  SegmentE.output();
  SegmentF.output();
  SegmentG.output();
  SegmentDP.output();
}

void allSegmentInput() {
  SegmentA.input();
  SegmentB.input();
  SegmentC.input();
  SegmentD.input();
  SegmentE.input();
  SegmentF.input();
  SegmentG.input();
  SegmentDP.input();
}

void allDigitOutput() {
  Digit1.output();
  Digit2.output();
  Digit3.output();
  Digit4.output();
  Digit5.output();
  Digit6.output();
  Digit7.output();
  Digit8.output();
  Digit9.output();
}

void allDigitInput() {
  Digit1.input();
  Digit2.input();
  Digit3.input();
  Digit4.input();
  Digit5.input();
  Digit6.input();
  Digit7.input();
  Digit8.input();
  Digit9.input();
}

void allSegmentOff() {
  SegmentA.SegmentOff();
  SegmentB.SegmentOff();
  SegmentC.SegmentOff();
  SegmentD.SegmentOff();
  SegmentE.SegmentOff();
  SegmentF.SegmentOff();
  SegmentG.SegmentOff();
  SegmentDP.SegmentOff();
}

void allDigitOff() {
  Digit1.DigitOff();
  Digit2.DigitOff();
  Digit3.DigitOff();
  Digit4.DigitOff();
  Digit5.DigitOff();
  Digit6.DigitOff();
  Digit7.DigitOff();
  Digit8.DigitOff();
  Digit9.DigitOff();
}

void selectDigit(byte digit) {

  allDigitOff();
  allDigitInput();

  switch (digit) {
    case 1:
      Digit1.DigitOn();
      Digit1.output();
      break;
    case 2:
      Digit2.DigitOn();
      Digit2.output();
      break;
    case 3:
      Digit3.DigitOn();
      Digit3.output();
      break;
    case 4:
      Digit4.DigitOn();
      Digit4.output();
      break;
    case 5:
      Digit5.DigitOn();
      Digit5.output();
      break;
    case 6:
      Digit6.DigitOn();
      Digit6.output();
      break;
    case 7:
      Digit7.DigitOn();
      Digit7.output();
      break;
    case 8:
      Digit8.DigitOn();
      Digit8.output();
      break;
    case 9:
      Digit9.DigitOn();
      Digit9.output();
      break;
  }
}

void display(byte raw)
{
  if (raw & B01000000)
  {
    SegmentA.SegmentOn();
  }
  else
  {
    SegmentA.SegmentOff();
  }

  if (raw & B00100000)
  {
    SegmentG.SegmentOn();
  }
  else
  {
    SegmentG.SegmentOff();
  }

  if (raw & B00010000)
  {
    SegmentD.SegmentOn();
  }
  else
  {
    SegmentD.SegmentOff();
  }

  if (raw & B00001000)
  {
    SegmentB.SegmentOn();
  }
  else
  {
    SegmentB.SegmentOff();
  }

  if (raw & B00000100)
  {
    SegmentC.SegmentOn();
  }
  else
  {
    SegmentC.SegmentOff();
  }

  if (raw & B00000010)
  {
    SegmentF.SegmentOn();
  }
  else
  {
    SegmentF.SegmentOff();
  }

  if (raw & B00000001)
  {
    SegmentE.SegmentOn();
  }
  else
  {
    SegmentE.SegmentOff();
  }

  Digit1.DigitOn();

}

byte displayv[] = {47, 111, 107, 107, 62, 121, 95, 12, 127};

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);

  allSegmentOff();
  allDigitOff();
  allSegmentOutput();
  allDigitOutput();
}

unsigned int framecnt = 64000; // initial delay while it overflows past 0
byte frame = 32;

void loop() {
  // put your main code here, to run repeatedly:

  byte raw;

  for (byte i = 0; i < 9; i++)
  {
    display(displayv[i]);
    selectDigit(i + 1);
    delayMicroseconds(350);
  }

  if (frame < 36)
  {
    framecnt++;
  }

  if (framecnt == 350)
  {
    framecnt = 0;
    frame++;
    displayv[8] = pgm_read_byte(DIGITSTABLEUP + frame);
  }
}