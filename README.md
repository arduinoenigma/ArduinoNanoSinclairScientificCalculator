# ArduinoNanoSinclairScientificCalculator
An Arduino port of Ken Shirriff Sinclair Scientific Calculator Simulator http://righto.com/sinclair


--- versions left for historical reasons<br>
SinclairScientific1, master branch for v1,v2 and v3 boards, updated to latest and greatest, all digits working, timing adjusted to match real calculator
SinclairScientificExperiments, an old snapshot of the SinclairScientific tree with ALU functions implemented.<br>
SinclairScientificExperiments2, a lean and mean and slow, no Serial.print, running version of the calculator, needs more step(), less display()<br>
SinclairScientific5, master branch for v5 boards, the keyboard and display circuit match the real Sinclair Scientific Calculator. See below for latest version.
<br>--- end of historical versions

SinclairScientific7, master branch for V7 and V5 boards. Selectable via a #define statement. Download this one and configure the #defines for your board.

This simulator is what powers the following emulator:
https://www.tindie.com/products/ArduinoEnigma/sinclair-scientific-calculator-emulator/

Board V5:<br>
https://oshpark.com/projects/OSfM0RLN<br>
Board V2:<br>
https://oshpark.com/projects/zOpXIok8<br>
